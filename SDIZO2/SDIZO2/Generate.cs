﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SDIZO2
{
    class Generate
    {
       static int maxEdgeQuantity;//maksymalna iloc krawedzi

        public  int[] NewGraph(int start, int end, int vortexQuantity,bool directed, double density)
        {
            int edgeCounter = 0;//licznik krawedzi
            int[,] matrix = new int[vortexQuantity, vortexQuantity];//macierz o wymiarach ilosc krawedzi x ilosc krawedzi
            List<int[]> GList = new List<int[]>();//lista
            Random rand = new Random();//zmienna do losowania wartosci

            //obliczamy maxymalna ilosc krawedzi
            if(directed)//jezeli graf skierowany
            {
                maxEdgeQuantity = Convert.ToInt32(density * (vortexQuantity * (vortexQuantity - 1)));
            }
            else//graf nieskierowany
            {
                maxEdgeQuantity = Convert.ToInt32((density * (vortexQuantity * (vortexQuantity - 1))) /2);
            }

            for (int i = 0; i < vortexQuantity; i++)//wypelnienie macierzy umowna wartoscia oo(maksymalna wartosc int) - brak krawedzi
            {
                for (int j = 0; j < vortexQuantity; j++)
                {
                    matrix[i, j] = Int32.MaxValue;
                }
            }

            for(int i=0;i<vortexQuantity-1;)
            {//petla przechodzi przez wszystkie wierzcholki
                matrix[i, i + 1] = rand.Next(1, vortexQuantity);//losujemy wage kraedzi z przedzialu (1,ilosc wierzch)
                edgeCounter++;//zwiekszamy licznik krawedzi
                i ++;
            }
            double ratio = Convert.ToDouble(edgeCounter) /Convert.ToDouble(maxEdgeQuantity);

            if(edgeCounter<maxEdgeQuantity)
            {//losowanie do odpowiedniej gestosci grafu
                
                int range2 = rand.Next(0, vortexQuantity);//losujemy wierzcholek koncowy
                int range = rand.Next(0, vortexQuantity);//losujemy wierzcholek poczatkowy

                while (ratio<density)
                {
                        if (matrix[range, range2] == Int32.MaxValue)//jezeli wylosowana krawedz jeszcze nie istnieje
                        {
                            matrix[range, range2] = rand.Next(1, vortexQuantity);//losujemy wage kraedzi z przedzialu (1,ilosc wierzch)
                            edgeCounter++;//zwiekszamy licznik krawedzi
                            ratio = Convert.ToDouble(edgeCounter) / Convert.ToDouble(maxEdgeQuantity);//obliczamy aktualna gestosc
                        }
                        while (matrix[range, range2] != Int32.MaxValue)//losujemy dopoki wylosowana krawedz bedzie krawedzia jeszcze nie istnijaca
                        {
                            range = rand.Next(0, vortexQuantity);
                            range2 = rand.Next(0, vortexQuantity);
                        while(range == range2)//jezeli wylosowalismy cykl losujemy dalej
                        {
                            range = rand.Next(0, vortexQuantity);
                            range2 = rand.Next(0, vortexQuantity);
                        }
                        }
                }
            }

            return FillArr(edgeCounter, vortexQuantity, start, end,matrix);
        }

        private static int[] FillArr(int edgeCounter,int vortexQuantity,int start, int end, int[,]matrix)
        {//funkcja wypelnia i zwraca tablice z grafem
            int[] filledArr = new int[4 + edgeCounter * 3];//twozymy nowa tablice o wymiarach potrzebnych na przechowanie wszystkich danych
            int startIndex = 4;//od 4 indexu bedziemy kopiowac do tablicy

            //tablice wypelniamy tak aby spelniala normy narzuceone w pliku, ponizej dane do pierwszej linii
            filledArr[0] = edgeCounter;//na pierwsze miejsce ilosc krawedzi
            filledArr[1] = vortexQuantity;//na drugie ilosc wierzcholkow
            filledArr[2] = start;//na 3 wierzcholek poczatkowy
            filledArr[3] = end;//na 4 wierzcholek koncowy

            //kopiowanie wartosci z macierzy do tablicy
            for(int i=0;i<vortexQuantity;i++)
            {
                for(int j=0;j<vortexQuantity;j++)
                {
                    if(matrix[i,j]!=Int32.MaxValue)//jezeli dana krawedz istnieje
                    {
                        filledArr[startIndex] = i;  startIndex++;//dodajemy wierzcholek poczatkowy i zwiekszamy index
                        filledArr[startIndex] = j; startIndex++;//dodajemy wierzcholek koncowy i zwiekszamy index
                        filledArr[startIndex] = matrix[i,j]; startIndex++;//dodajemy wage krawedzi i zwiekszamy index
                    }
                }
            }

            return filledArr;//zwracamy tablice
        }
        public static int GetMaxEdgeQuantity()
        {//zwracamy max ilosc krawedzi
            return maxEdgeQuantity;
        }
    }
}
