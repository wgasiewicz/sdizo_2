﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SDIZO2
{
    class Dijkstr
    {
        int[] dist;//wagi krawedzi
        int[] prev;//poprzednicy
        List<bool> checkedVortexes;//przebadane wierzcholki true- przebadany false -nie
        bool workMode;//tryb pracy
        Graph dgraph;
        int indexToWork = 0;//aktualny index wierzcholka

        public Dijkstr(bool mode, Graph graph)
        {
            dgraph = graph;
            workMode = mode;//ustawiamy tryb pracy
            checkedVortexes = new List<bool>();//inicjalizujemy talice z przebadanymi wierzcholkami
            dist = new int[graph.VortexQuantity];//tworzymy tablice na wagi krawedzi
            prev = new int[graph.VortexQuantity];//tworzymy tablice na poprzednikow

            for (int i = 0; i < graph.VortexQuantity; i++)//na poczatku wszystkie wierzcholki nalezy przebadac
            { checkedVortexes.Add(false);
            }
            
            for(int i=0;i<dist.Length;i++)
            {
                dist[i] = Int32.MaxValue;//wypelniamy odleglosci wartoscia oo
                prev[i] = Int32.MinValue;//wypelniamy poprzednikow wartoscia -oo (brak poprzednika)
            }
            dist[0] = 0;
        }

        public void Algorythm()//funkcja wykonuje algorytm wyszukania najkrotszej sciezki z poczatkowego do ostatnigo wierzcholka
        {
            while (checkedVortexes.Contains(false))//dopoki ktorys z wierzcholkow nie zostal przebadany
            {
                FindMinDistance();//wyszukujemy najkrotsza droge
                PrepareVortexes();//sprawdzamy czy poprawi ona dotychczasowy wynik
            }
        }
        private void FindMinDistance()
        {//wyszukiwanie aktualnej najkrotszej drogi
            int toCompare = Int32.MaxValue;
            for (int i=0;i<dgraph.VortexQuantity;i++)
            {
                if(checkedVortexes[i]==false && dist[i]<toCompare)
                {
                    toCompare = dist[i];
                    indexToWork = i;
                }
            }
        }
        private void PrepareVortexes()
        {
            checkedVortexes[indexToWork] = true;//aktualny wierzcholek ustawiamy jako odwiedzony
            if (workMode==false )//praca w trybie macierzowym
            {
                for(int i=0;i<dgraph.VortexQuantity;i++)
                {
                    if(dgraph.Matrix[indexToWork,i]!=Int32.MaxValue)//jezeli dana krawedz istnieje
                    {
                        if(dgraph.Matrix[indexToWork,i]+dist[indexToWork]<dist[i])//jezeli z nowa krawedzia jestesmy w stanie poprawic aktualny wynik
                        {
                            dist[i] = dgraph.Matrix[indexToWork, i] + dist[indexToWork];//zapisujemy lepszy wynik
                            prev[i] = indexToWork;//zapisujemy nowego poprzednika
                        }
                    }
                }
            }
            else//praca w trybie listowym
            {
                for (int index = 0; index < dgraph.GList[indexToWork].Count;)//przechodzimy cala liste zewnetrzna
                {
                    int i = dgraph.GList[indexToWork][index];//pobieramy wierzcholek docelowy
                    index++;//zwiekszamy index
                    int weight = dgraph.GList[indexToWork][index];//pobieramy wage krawedzi
                    index++;//zwiekszamy index
                    if (weight + dist[indexToWork] < dist[i])//jezeli z nowa krawedzia jestesmy w stanie poprawic aktualn wynik
                    {
                        dist[i] = weight + dist[indexToWork];//zapisujemy lepszy wynik
                        prev[i] = indexToWork;//zapisujemy nowego poprzednika
                    }
                }               
            }
        }
        private static string Reverse(string s)
        {//funkcja odwraca kolejnosc znakow w stringu
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
       public string Show()//funkcja wyswietla wynik algorytmu
        {
            string type1 = "listowym"; string type2 = "macierzowym";

            string show = "ALGORYTM DIJKSTRY w trybie ";
            string temp = "";
            int control=0;
            if (workMode == false) show += type2 + Environment.NewLine;//wybieramy odpowiedni tryb algorytmu
            else show += type1 + Environment.NewLine;
            for(int i=0;i<dist.Length;i++)
            {
                int pathread = prev[i];//zmienna do odczytnia sciezki
                show += i+" | "+dist[i]+" | ";
              while(pathread!=Int32.MinValue)
                {
                    control = pathread;
                    temp += pathread+ " ";
                    pathread = prev[pathread];
                }
                show += Reverse(temp)+" "+i+Environment.NewLine;
                temp = "";
            }
            
            return show;//zwracamy gotowy string
        }
    }
}
