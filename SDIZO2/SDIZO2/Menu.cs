﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace SDIZO2
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {//wyswietlamy menu generowania grafu
            GenerateMenu gm = new GenerateMenu();
            gm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {//wyswietlamy menu najkrotszj sciezki
            AlgorythmMenu spm = new AlgorythmMenu();
            spm.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }
    }
}
