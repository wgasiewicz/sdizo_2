﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
namespace SDIZO2
{
    class Functions
    {
        public string ChooseFile()
        {
            //wyierz plik z danymi
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.ShowDialog();
            return ofd.FileName;// zapisanie sciezki do pliku
        }
        public Boolean IsNumeric(string text)
        {
            int number;
            bool isNumber = int.TryParse(text, out number);//sprawdzenie czy wartosc podana jst liczba int
            return isNumber;
        }
        
    }
}
