﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SDIZO2
{
    class KruskalSet
    {
        private NodeKruskalSet[] nodes;//tablica wezlow zbioru

        public KruskalSet(int vortexQuantity)
        {
            // Tworzenie zbioru dla każdego wierzchołka.
            nodes = new NodeKruskalSet[vortexQuantity];
            for (int i = 0; i < vortexQuantity; i++)
            {
                nodes[i] = new NodeKruskalSet();
            }
        }
        
        public int FindSet(int vortex)
        {//funkcja wyszukuje zbior dla danego wierzcholka

            if (nodes[vortex].up != vortex)
            {
                nodes[vortex].up = FindSet(nodes[vortex].up);
            }

            return nodes[vortex].up;
        }
        public void MakeSet(int vertex)
        {//funkcja tworzy zbior
            nodes[vertex].up = vertex;
            nodes[vertex].amount = 0;
        }

        public void UnionSets(int start, int end)
        {//funkcja lczy dwa zbiory
            int setA, setB;
            
            setA = FindSet(start);//wyszukujemy zbior A
            setB = FindSet(end);//wyszukujemy zbior B

            if (setA != setB)//jezeli zbiory sa rozne mozemy je polczaczyc
            {
                if (nodes[setA].amount > nodes[setB].amount)//jezeli zbior A jest wiekszy od zbioru B 
                {
                    nodes[setB].up = setA;//zbior mniejszy wpisujemy do wiekszego
                }
                else//jezeli zbior B jest wiekszy
                {
                    nodes[setA].up = setB;
                    if (nodes[setA].amount == nodes[setB].amount)//jezeli zbiory sa tego samego rozmiaru
                    {
                        nodes[setB].amount++;//zwiekszamy rozmiar zbioru
                    }
                }
            }
        }
    }
}
