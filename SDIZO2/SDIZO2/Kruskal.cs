﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SDIZO2
{
    class Kruskal
    {
        Graph kgraph;
        List<bool> checkedVortexes;//przebadane wierzcholki true- przebadany false -nie
        List<int> sortedEdges;//uporzadkowane rosnaco wagi grafu
        bool algMode;//tryb pracy 
        int[,] connections;//tablica przechowujaca wszystkie polaczenia w grafie
        int[,] connectionsList;//tablica na polaczenia z listy
        int[,] result;//wynik
        int[,] sortedCon;//tablica z posortowanymi polaczeniami
        int[,] sortedConList;//posortowane polaczenia z listy
        KruskalSet set;

        public Kruskal(bool mode, Graph graph)//konstruktor - przyjmuje tryb pracy i graf
        {
            kgraph = graph;
            set = new KruskalSet(graph.VortexQuantity);
            sortedEdges = new List<int>();
            algMode = mode;//ustawiamy tryb algorytmu
            checkedVortexes = new List<bool>();
            result = new int[3, graph.EdgeQuantity];//tablica na wyniki
            connections = new int[3, graph.EdgeQuantity];//tablica zawiera dane z placzeniami krawedzi: zkad dokad waga
            connectionsList = new int[3, graph.EdgeQuantity*2];//tablica na polaczenia pobrane z listy
            sortedCon= new int[3, kgraph.EdgeQuantity];//tablica na posortowane polaczenia
            sortedConList = new int[3, kgraph.EdgeQuantity * 2];//tablica na posortowane polaczenia z list

            for (int i=0;i<graph.VortexQuantity;i++)//wypelnianie listy wagami krawedzi
            {
                set.MakeSet(i);
                checkedVortexes.Add(false);//wypelniamy stany sprawdzonych wierzcholkow
                for (int j = 0; j < i; j++)
                {
                    if (graph.Matrix[i, j] != Int32.MaxValue)
                    {
                        sortedEdges.Add(graph.Matrix[i, j]);//dodajemy wage krawedzi do puli istniejacych
                    }
                }
            }         
        
            sortedEdges.Sort();//sortujemy liste z krawedziami
        }
        //algorytm
        public void Algorithm()
        {
            int count;//index pomoniczy do wypelniania tablicy wynikow
           if(algMode)//jezeli algMode=true pacujemy w trybie listowym
            {
                FillList();//wypelniamy tablice polaczen wartosciami z listy
                SortListCon();//sortujemy polaczenia
                DeleteDuplicateFromList();//usuwamy zduplikowane krawedzie
                count = 0;
                while(checkedVortexes.Contains(false))
                {
                    for (int i = 0; i < kgraph.EdgeQuantity - 1; i++)
                    {
                        if (set.FindSet(sortedCon[0, i]) == set.FindSet(sortedCon[1, i])) { }// jezeli zbiory dla poczatkowej i koncowej krawedzi sa identyczne pomijamy je poniewaz wystepuje cykl
                        else//jezeli cykl nie wystepuje
                        {
                            set.UnionSets(sortedCon[0, i], sortedCon[1, i]);//laczymy zbiory dla obu krawedzi
                            result[0, count] = sortedCon[0, i];//doajemy poczatek krawedzi do wyniku
                            result[1, count] = sortedCon[1, i];//dodajemy koniec kawedzi do wyniku
                            result[2, count] = sortedCon[2, i];//dodajemy wage krawedzi do wyniku
                            checkedVortexes[sortedCon[0, i]] = true;//ustawiamy poczatkowy wierzholek jako sprawdzony
                            checkedVortexes[sortedCon[1, i]] = true;//ustawiamy koncowy wierzcholek jako sprawdzony
                            count++;//zwiekszamy licznik
                        }

                    }

                }
            }
            else//praca w trybie macierzowym
            {
                FillMatrix();//wypelniamy tablice polaczen danymi z macierzy
                Sort();//sortujemy polaczenia
                count = 0;//pomocniczy licznik
                while (checkedVortexes.Contains(false))//dopoki nie przebadamy wszystkich wirzcholkow
                {
                    for (int i = 0; i < kgraph.EdgeQuantity-1; i++)//dla ilosc krawedzi - 1 
                    {
                      if (set.FindSet(sortedCon[0,i]) == set.FindSet(sortedCon[1, i])) { }//jezeli zbiory dla poczatkowej i koncowej krawedzi sa identyczne pomijamy je poniewaz wystepuje cykl
                       else//jezeli cykl nie wystepuje
                          {
                            set.UnionSets(sortedCon[0, i], sortedCon[1, i]);//laczymy zbiory dla obu krawedzi
                            result[0, count] = sortedCon[0, i];//dodajemy poczatek krawedzi do wyniku
                            result[1, count] = sortedCon[1, i];//dodajemy koniec krawedzi do wyniku
                            result[2, count] = sortedCon[2,i];//dodajemy wage krawedzi do wyniku
                            checkedVortexes[sortedCon[0, i]] = true;//ustawiamy poczatkowy wierzcholek jako sprawdzoony 
                            checkedVortexes[sortedCon[1, i]] = true;//ustawiamy koncowy wierzcholek jako sprawdzamy
                            count++;//zwiekszamy index pomocniczy
                       }
                       
                    }
                }
            }
        }
        private void FillMatrix()
        {            //wypelnianie tablicy polaczen danymi
            int fill = 0;
            for (int i = 0; i < kgraph.VortexQuantity; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    if (kgraph.Matrix[i, j] != Int32.MaxValue)
                    {
                        connections[0, fill] = j;//poczatek
                        connections[1, fill] = i;//koniec
                        connections[2, fill] = kgraph.Matrix[i, j];//waga
                        fill++;//zwiekszami licznik
                    }
                }
            }
        }
        private void FillList()
        {//wypelnienie tablicy danymi z listy         
            int fill = 0;//licznik pomocniczy do wypelniania tablic
            for(int indexToWork=0;indexToWork<kgraph.VortexQuantity;indexToWork++)
            for (int index = 0; index < kgraph.GList[indexToWork].Count;)
            {
                int i = kgraph.GList[indexToWork][index];//pobieramy docelowy wierzcholek
                index++;//zwiekszamy index
                int weight = kgraph.GList[indexToWork][index];//pobieramy wage krawedzi
                index++;//zwiekszamy index
                connectionsList[0, fill] = indexToWork;//poczatek
                connectionsList[1, fill] = i;//koniec
                connectionsList[2, fill] = weight;//waga
                fill++;//zwiekszamy licznik
            }
        }
        private void DeleteDuplicateFromList()
        {//funkcja usuwa zduplikowane krawedzie w liscie
            int from, to, weigth;//wierzcholek pozatkowy,koncowy, waga
            for (int i = 0; i < sortedConList.Length / 3; i++)//przechodzimy cala listy polaczen
            {
                from = sortedConList[0, i];//wybieramy kolejne poczatki krawedzi
                to = sortedConList[1, i];//konce krawedzi
                weigth = sortedConList[2, i];//wagi krawedzi
                for (int j = 0; j < sortedConList.Length / 3; j++)//ponownie przechozimi cala liste polaczen
                {
                    if (sortedConList[1, j] == from && sortedConList[0, j] == to && sortedConList[2, j] == weigth)//jezeli natrafimy na zduplikowana krawedz
                    {//jej wartosci ustawiamy na oo
                        sortedConList[0, j] = Int32.MaxValue;
                        sortedConList[1, j] = Int32.MaxValue;
                        sortedConList[2, j] = Int32.MaxValue;
                    }
                }
            }
            int fill = 0;//index pomocniczy do wypelniania
            for (int i = 0; i < sortedConList.Length / 3; i++)//przechodzimy cala liste polaczen
            {
                if(sortedConList[0,i]!=Int32.MaxValue && sortedConList[1,i]!=Int32.MaxValue && sortedConList[2,i]!=Int32.MaxValue)//jezeli krawedz nie byla zduplikowana
                {
                    sortedCon[0, fill] = sortedConList[0, i];//wpisujemy jej wierzcholek pozatkowy do tablicy posortowanych polaczen
                    sortedCon[1, fill] = sortedConList[1, i];//wpisujemy jej wierzcholek koncowy do tablicy polaczen
                    sortedCon[2, fill] = sortedConList[2, i];//wpisujemy jej wage do tablicy polaczen
                    fill++;//zwiekszamy index pomocniczy
                }                        
            }
        }
        private void Sort()
        {//funkja sortuje krawedzie od najmniejszej do najwiekszej wagi dla macierzy
            int minValue, minBefore=0;//zmienne na minimalna wartosc krawedzi oraz poprzednia minimalna wartosc krawedzi
                int x=0;//index pomocniczy do wypelniania tablicy posortowanych polaczen
            
            for (int i = 0; i < sortedEdges.Count; i++)//dla kazdego z wierzcholkow
            {
                minValue = sortedEdges[i];//pobieramy z listy posortowanych wierzcholkow kolejne
                for (int j=0;j<kgraph.EdgeQuantity;j++)
                {
                    if (minValue!=minBefore && connections[2, j] == minValue)//jezeli aktualna najmniejsza wartosc != od popzedniej i waga aktualnego polaczenia jest rowna aktualnej najmniejszej watosci
                    {
                        sortedCon[0, x] = connections[0, j];//zapisujemy wirzcholek poczatkowy krawedzi o najmniejszej wadze
                        sortedCon[1, x] = connections[1,j];//zapisujemy wierzcholek koncowy krawedzi o najmniejszej wadze
                        sortedCon[2, x] = connections[2, j];//zapiujemy wage
                        x++;//zwiekszamy index pomocniczy
                    }
                }
                minBefore = minValue;//ustawiamy poprzednia wartosc minimalna
            }
        }
        private void SortListCon()
        {//funkja sortuje krawedzie od najmniejszej do najwiekszej wagi dla listy
            int minValue, minBefore = 0;
            int x = 0;

            for (int i = 0; i < sortedEdges.Count; i++)
            {
                minValue = sortedEdges[i];
                for (int j = 0; j < connectionsList.Length/3; j++)
                {
                    if (minValue != minBefore && connectionsList[2, j] == minValue)
                    {
                        sortedConList[0, x] = connectionsList[0, j];
                        sortedConList[1, x] = connectionsList[1, j];
                        sortedConList[2, x] = connectionsList[2, j];
                        x++;
                    }
                }
                minBefore = minValue;
            }
        }
        public string Show()//funkcja wyswietla wynik algorytmu
        {
            int sum=0;
            string show = "ALGORYTM KRUSKALA W TRYBIE ";
            if (algMode)
                show += "LISTOWYM";
            else
                show += " MACIERZOWYM";
            show += Environment.NewLine;

            for(int i=0;i<kgraph.EdgeQuantity;i++)
            {
                if (result[0, i] != 0 || result[1, i] != 0)
                {
                    show += "(" + result[0, i] + "," + result[1, i] + ") " + result[2, i] + Environment.NewLine;
                    sum += result[2, i];
                }
            }
            show += "MST=" + sum;
                    return show;
        }
    }
}
