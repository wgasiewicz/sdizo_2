﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SDIZO2
{
    class Graph
    {
        public int EdgeQuantity; //ilosc krawedzi
        public int VortexQuantity;//ilosc wierzcholkow
        public int[,] Matrix;//macierz sasiedztwa
        public List<List<int>> GList;
        public bool Directed;//czy graf skierowany

        public Graph(bool isDirected, int [] inArray)
        {
            EdgeQuantity = inArray[0];//ilosc krawedzi
            VortexQuantity = inArray[1];//ilosc wierzcholkow
            Directed = isDirected;//czy graf skieowany
            
            GList = new List<List<int>>(VortexQuantity);
            for (int i=0;i<VortexQuantity;i++)//dodajemy do glownej listy listwy wewnetrzne
            {
                GList.Add(new List<int>());
            }

            //zczytywanie danych z tablicy z pomienieciem inf o liczbie krawedzi wierzcholkow itd
            int[] arrToFill = new int[inArray.Length - 4];
            for(int i=0; i<arrToFill.Length; i++)
            {
                arrToFill[i] = inArray[i + 4];
            }

            Matrix = new int[VortexQuantity, VortexQuantity];//tworzenie macierzy o wymiarach ilosc wierzch. x ilosc wierzch.
            for(int i=0;i<VortexQuantity;i++)//wypelnienie mcierzy umowna wartoscia oo - brak krawedzi
            {
                for(int j=0;j<VortexQuantity;j++)
                {
                    Matrix[i, j] = Int32.MaxValue;
                }
            }
            FillGraph(arrToFill);
        }
        //funkcja wypelniajaca graf
        private void FillGraph(int[] vortexArr)
        {
            for (int i = 0; i < EdgeQuantity * 3; i += 3)
            {
                FillList(vortexArr[i], vortexArr[i + 1], vortexArr[i + 2]);
                FillMatrix(vortexArr[i], vortexArr[i + 1], vortexArr[i + 2]);
            }
        }
        //funkcja wypelniajace liste
        private void FillList(int startVortex, int endVortex, int edgeValue)
        {
            if(Directed)//jesli graf skierowany
            {
                FillInsideList(startVortex, endVortex);
                FillInsideList(startVortex, edgeValue);
            }
            else
            {//dodajemy droge w jedna strone
                FillInsideList(startVortex, endVortex);
                FillInsideList(startVortex, edgeValue);
                //dodajemy droge w odwrotnej kolejnosci, poniewaz graf nieskirowany
                FillInsideList(endVortex, startVortex);
                FillInsideList(endVortex, edgeValue);
            }
        }
        //funkcja wypelniajaca wewnetrzna liste
        private void FillInsideList(int indexWanted,int valueToAdd)
        {
            foreach(List<int> inside in GList)
            {
                if (GList.IndexOf(inside)==indexWanted)
                    inside.Add(valueToAdd);
            }
        }
        //funkcja wypelniajaca macierz
        private void FillMatrix(int startVortex, int endVortex, int edgeValue)
        {
            if (Directed)//jesli graf skierowany
            {
                Matrix[startVortex, endVortex] = edgeValue;//dodajemy droge w jedna strone
            }
            else
            {//doajemy drogi w obydwie strony
                Matrix[startVortex, endVortex] = edgeValue;
                Matrix[endVortex, startVortex] = edgeValue;
            }
        }
        public string GraphToMatrix()//wyswitlanie grafu w fomie mcierzy
        {
            string show="MACIERZ SĄSIADÓW: "+Environment.NewLine + "Ilość wierzchołków = " + VortexQuantity + " Ilość krawędzi = " + EdgeQuantity + " Maksymalna ilość krawędzi =  "+Generate.GetMaxEdgeQuantity()+Environment.NewLine;
            show += "       ";
            for (int i = 0; i < VortexQuantity; i++)
                show += "(" + i + ") ";
            show += Environment.NewLine;
            string temp="";//pomocniczy string do zczytywania kolejnych linii
            for(int i=0;i<VortexQuantity;i++)
            {
                temp = "(" + i + ")->";//wierzcholki
                for(int j=0;j<VortexQuantity;j++)
                {
                    if(Matrix[i,j]==Int32.MaxValue)//jezeli w macierzy na danym msc byla oo
                    {
                        temp += "[ - ] ";//miejsce bedzie puste
                    }
                    else
                    {
                        temp += "["+Matrix[i, j] + "] ";
                    }
                }
                show += temp + Environment.NewLine;
            }

            return show;
        }
        //funkcja przygotowuje graf do wyswietlenia w formie listy sasiadow
        public string GraphToList()
        {
            int i = 0;//licznik wierzcholkow
            int counter = 0;//pomocniczy liznik do dodawania odpowiednich znakow przy wyswietlaniu
            string show = "LISTA SĄSIEDZTWA: " + Environment.NewLine+"Ilość wierzchołków = "+VortexQuantity+ " Ilość krawędzi = " + EdgeQuantity+ " Maksymalna ilość krawędzi =  " + Generate.GetMaxEdgeQuantity() + Environment.NewLine;
            string temp = "";

            foreach(List<int> inside in GList)
            {
                temp = "(" + i + ")->";
                i++;
                counter = 0;
                foreach (int elem in inside)
                {
                    temp +=elem;
                    if (counter % 2 == 0)
                    {
                        temp += "|";
                    }
                    else temp += " ";
                    counter++;
                }
                show += temp + Environment.NewLine;
            }
                return show;
        }
        
    }
}
