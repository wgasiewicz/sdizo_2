﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SDIZO2
{
    public partial class AlgorythmMenu : Form
    {
        Generate gen = new Generate();
        private int[] arr;//tablica na dane odczytane z pliku
        private Graph graph;
        private Dijkstr dijkstra;
        private Kruskal kruskal;
        private Functions getFilePath= new Functions();
        private Functions call = new Functions();
        private bool algMode;//tryb dzialania algorytmow true- listowy false -macierzowy
        private bool testMode = false;//tryb testu
        private double testTime;//czas trybu testowego
        private bool shortestPathClicked = false;//czy do testow wybrano najkrotsza sciezke
        private bool notToDODijkstra = true;//zabezpieczenie przed ujemnymi krawedziami
        public AlgorythmMenu()
        {
            InitializeComponent();
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void button3_Click(object sender, EventArgs e)
        {//wczytywanie grafu
            ReadFromFile(getFilePath.ChooseFile());
        }

        private void button4_Click(object sender, EventArgs e)
        {//wypisz listownie
            if (graph != null)//jezeli graf istnieje
            {
                ShowResult sr = new ShowResult(graph.GraphToList());
                sr.ShowDialog();
            }
            else
            {
                MessageBox.Show("Graf jest pusty!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {//wypisz macierzowo
            if(graph!=null)//jezeli graf istnieje
            {
                ShowResult sr = new ShowResult(graph.GraphToMatrix());
                sr.ShowDialog();
            }
            else//bład
            {
                MessageBox.Show("Graf jest pusty!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {//czas operacji drzewa mst

        }

        private void AlgorythmMenu_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {//czas operacji najkrotszej sciezki

        }

        private void button5_Click(object sender, EventArgs e)
        {//wypisz drzewo mst
            if (graph != null)//jezeli graf istnieje
            {                
                ShowResult sr = new ShowResult(kruskal.Show());
                sr.ShowDialog();
            }
            else//bład
            {
                MessageBox.Show("Graf jest pusty!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {//start drzewo mst
            notToDODijkstra = true;//wyrano mst do testow
            if (comboBox2.SelectedItem == null || graph == null)//jezeli nie wybrano trybu pracy lub graf nie istnieje
            {
                MessageBox.Show("Nie wybrano trybu pracy lub grafu!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(radioButton2.Checked == false)//jezeli nie wybrano algorytmu
            {
                MessageBox.Show("Nie wybrano algorytmu!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                if (comboBox2.SelectedItem.Equals("Listowy"))//ustawiamy tryb algorytmu
                    algMode = true;//tryb listowy
                else
                    algMode = false;//tryb macierzowy
                kruskal = new Kruskal(algMode, graph);
                Stopwatch stopwatch = Stopwatch.StartNew();//start pomiaru czasu
                kruskal.Algorithm();//wywolanie algorytmu kruskala
                stopwatch.Stop();//zatrzymanie pomiaru czasu
                textBox2.Text = stopwatch.Elapsed.TotalMilliseconds.ToString();
                if (testMode) testTime += stopwatch.Elapsed.TotalMilliseconds;//jezeli jestesmy w trybie testowym sumujemy czasy poszczegolnych wynikow
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {//wypisz najkrotsza sciezka
            if (graph != null)//jezeli graf istnieje
            {
                ShowResult sr = new ShowResult(dijkstra.Show());//wyswietlanie wyniku algorytmu
                sr.ShowDialog();
            }
            else//bład
            {
                MessageBox.Show("Graf jest pusty!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {//start najkrotsza sciezka
            if (comboBox2.SelectedItem==null || graph==null)//jezeli nie wybrano trybu pracy lub graf nie istnieje
            {
                MessageBox.Show("Nie wybrano trybu pracy lub grafu!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }//jezeli nie wybrano algorytmu
            else if (radioButton1.Checked == false)
            {
                MessageBox.Show("Nie wybrano algorytmu!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if(notToDODijkstra==false)//jezeli we wczytanym grafie znalzla sie choc 1 krawedz z ujemna waga nie mozna wykonc algorytmu
            {
                MessageBox.Show("Wybrany graf zwiera ujemne kawedzie!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                shortestPathClicked = true;//wybrano do testow
                if (comboBox2.SelectedItem.Equals("Listowy"))//ustawiamy tryb algorytmu
                    algMode = true;
                else
                    algMode = false;
                dijkstra = new Dijkstr(algMode, graph);
                Stopwatch stopwatch = Stopwatch.StartNew();//start pomiaru czasu
                dijkstra.Algorythm();//wywolanie algorytmu dijkstry
                stopwatch.Stop();//stop pomiaru czasu
                textBox1.Text = stopwatch.Elapsed.TotalMilliseconds.ToString();//wyswietlamy czas trwania algorytmu
                if (testMode) testTime += stopwatch.Elapsed.TotalMilliseconds;//jezeli tryb testowy aktywny sumujemy poszczegolne czasy
            }
        }
        
        private void button8_Click(object sender, EventArgs e)
        {//generuj graf
            if (comboBox1.SelectedItem != null && call.IsNumeric(textBox3.Text) == true)
            {
                if (Convert.ToInt32(textBox3.Text) >= 0)//jezeli ilosc wierzcholkow >=0
                {
                    string temp = comboBox1.SelectedItem.ToString();//zczytujemy wybrana gestosc
                    double density = Convert.ToDouble(temp);//zamieniamy ja na string
                    arr = gen.NewGraph(0, 0, Convert.ToInt32(textBox3.Text), checkBox1.Checked, density);//generujemy dane do grafu o wybranych parametach
                    graph = new Graph(checkBox1.Checked, arr);//tworzymy nowy graf
                   if(!testMode) MessageBox.Show("Wygenerowano pomyślnie!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Ilość krawędzi nie może być ujemna!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    textBox3.Clear();
                }
            }
            else
            {
                MessageBox.Show("Nie podano prawidłowych wartości do generowania!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox3.Clear();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {//przepustowosc

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {//ilosc wierzcholkow

        }
        private void ReadFromFile(string path)
        {//odczyt z pliku
            List<int> garr = new List<int>();//lista na pozostale dane dot. grafu
            string temp, temp2;//zmienne pomocnicze doodczytu lizb z pliku
            int space;//zminna na index wystapienia spacji
            if (path == "") return;
            StreamReader sr = new StreamReader(path);
            string line;//zmianna pomocnicza do odczytywania kolejnych linii z pliku
            int i = 0;//licznik linii
            while ((line = sr.ReadLine()) != null)//dopoki w danej linii jest jakas wartosc
            {
                if (i == 0)//dane z pierwszej linii
                {
                    for (int j = 0; j < 3; j++)
                    {                        
                        space = line.IndexOf(" ");
                        temp = line.Substring(space);
                        temp2 = line.Remove(line.Length - temp.Length, temp.Length);                        
                        line = line.Remove(0, line.Length - temp.Length+1);

                        if (call.IsNumeric(temp2) == false)//spawdzamy czy bedziemy dodawac liczbe
                        {
                            MessageBox.Show("Plik niepoprawny!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                        garr.Add(Convert.ToInt32(temp2));
                        if (j == 2)
                        {
                            garr.Add(Convert.ToInt32(line));
                        }
                    }
                }
                else
                {
                    for(int j=0;j<2;j++)
                    {
                        space = line.IndexOf(" ");
                        temp = line.Substring(space);
                        temp2 = line.Remove(line.Length - temp.Length, temp.Length);

                        if (call.IsNumeric(temp2) == false)//spawdzamy czy bedziemy dodawac liczbe
                        {
                            MessageBox.Show("Plik niepoprawny!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        line = line.Remove(0, line.Length - temp.Length + 1);
                        garr.Add(Convert.ToInt32(temp2));//dodajemy do listy
                        if (j == 1)
                        {
                            if (Convert.ToInt32(line) < 0) notToDODijkstra = false;
                            garr.Add(Convert.ToInt32(line));
                        }
                    }
                }
                i++;//zwiekzamy licznik linii
            }
            i = 0;//zerujemy licznik
            //wpisujemy dane z listy do talicy
            int c = 0;
            arr = new int[garr.Count];
            foreach (int elem in garr)
            {
               arr[c]=elem;
                c++;
            }
            graph = new Graph(checkBox1.Checked, arr);//tworzymy nowy graf z otrzymanych danych
            MessageBox.Show("Wczytano pomyślnie!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {//tryb pracy

        }

        private void button9_Click(object sender, EventArgs e)
        {//test 100x
            MessageBox.Show("Program wykona wybraną oprację 100 razy, za każdym razem generując nowy graf o wybranych parametrach. Uzyskane czasy są czasami średnimi!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            testMode = true;
            if (comboBox1.SelectedItem != null && call.IsNumeric(textBox3.Text) == true && comboBox2.SelectedItem != null)
            {
                if (Convert.ToInt32(textBox3.Text) >= 0)
                {
                    string temp = comboBox1.SelectedItem.ToString();
                    double density = Convert.ToDouble(temp);

                    arr = gen.NewGraph(0, 0, Convert.ToInt32(textBox3.Text), checkBox1.Checked, density);
                    graph = new Graph(checkBox1.Checked, arr);

                    if (shortestPathClicked)//do testow wybrano alg najkrotszej ciezki
                    {
                        for (int i = 0; i < 100; i++)//wykonujemy go 100 razy
                        {
                            button8.PerformClick();
                            button2.PerformClick();
                        }
                        textBox1.Text = (testTime / 100).ToString();
                    }
                    else//wybrano alg mst
                    {
                        radioButton2.Checked = true;
                        for (int i = 0; i < 100; i++)//wykonujemy 100 razy
                        {
                            button8.PerformClick();
                            button6.PerformClick();
                        }
                        textBox2.Text = (testTime / 100).ToString();
                    }
                    MessageBox.Show("Test przebiegł pomyślnie!", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Nie wybrano poprawnych ustawień!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }testMode = false; testTime = 0;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {//dijkstra

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {//kruskal

        }
    }
}
